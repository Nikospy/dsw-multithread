package com.dsw.pp2.cw3;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Third {
    public static final int SIZE = 100000000;
    private static long sum;

    
    public static void main(String[] args) throws InterruptedException {

        SumThread sumThread = new SumThread();
        long start = System.nanoTime();
        sumThread.start();
        sumThread.join();
        long end = System.nanoTime();
        System.out.println(sumThread.getResult());
        System.out.println("Multithread: " + (end - start));

        long start2 = System.nanoTime();
        singleAddAllToOne(SumThread.list);
        long end2 = System.nanoTime();
        System.out.println("Single:      " + (end2 - start2));
        System.out.println(getSum());
    }


    public static void randomValues(List<Integer> list) {
        for (int i = 0; i < SIZE; i++) {
            list.add(i, ThreadLocalRandom.current().nextInt(1, 100));
        }
    }

    public static void singleAddAllToOne(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
    }

    public static long getSum() {
        return sum;
    }
}
