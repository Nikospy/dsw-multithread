package com.dsw.pp2.cw3;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.dsw.pp2.cw3.Third.randomValues;

public class SumThread extends Thread {
    static List<Integer> list = Collections.synchronizedList(new ArrayList<>());

    {
        randomValues(list);
    }

    private static long result = 0;

    @Override
    public void run() {
        for (int i = 0; i < list.size(); i++) {
            result += list.get(i);
        }
    }

    public static long getResult() {
        return result;
    }
}

