package com.dsw.pp2.cw2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Second {
    private static final int SIZE = 1000000;

    public static void main(String[] args) throws InterruptedException {
        SafeList A = new SafeList();
        randomValues(A);
        SafeList B = new SafeList();
        randomValues(B);
        SafeList C = new SafeList();
        SafeList D = new SafeList();


        long start = System.nanoTime();
        threadSum(A, B, C);
        long end = System.nanoTime();
        System.out.println("Multithread: " + (end - start));

        long start2 = System.nanoTime();
        singleSum(A, B, D);
        long end2 = System.nanoTime();
        System.out.println("Single:      " + (end2 - start2));

    }



    public static void randomValues(SafeList list) {
        for (int i = 0; i < SIZE; i++) {
            list.add(i, ThreadLocalRandom.current().nextInt(1, 100000));
        }
    }

    public static void singleSum(SafeList A, SafeList B, SafeList D) {
        for (int i = 0; i < SIZE; i++) {
            D.add(i, A.get(i) + B.get(i));
        }
    }

    public static synchronized void threadSum(SafeList A, SafeList B, SafeList C) throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < SIZE; i++) {
                    C.add(i,(A.get(i)+B.get(i)));
                }
            }
        };

        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();


    }
}
